﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Google.Apis.Auth.OAuth2.Flows;
using Google.Apis.Auth.OAuth2.Responses;
using Microsoft.AspNetCore.Mvc;

namespace GmailApiSample.Controllers
{
    public class GoogleController : Controller
    {
        [HttpGet]
        public IActionResult Authorize(string code, string scope)
        {
            var flow = Program.GoogleAuthorizationCodeFlow;
            TokenResponse tokenResponse = flow.ExchangeCodeForTokenAsync(
                "user_test",
                code,
                "https://localhost:44380/Google/Authorize",
                CancellationToken.None).Result;
            // TODO Save Token
            return Json(tokenResponse);
        }
    }
}

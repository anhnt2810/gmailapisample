using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Auth.OAuth2.Flows;
using Google.Apis.Gmail.v1;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace GmailApiSample
{
    public class Program
    {
        public readonly static GoogleAuthorizationCodeFlow GoogleAuthorizationCodeFlow;
        static Program()
        {
            using var stream = new FileStream($@"{AppDomain.CurrentDomain.BaseDirectory}\credentials.json", FileMode.Open, FileAccess.Read);
            GoogleAuthorizationCodeFlow = new GoogleAuthorizationCodeFlow(new GoogleAuthorizationCodeFlow.Initializer()
            {
                ClientSecrets = GoogleClientSecrets.Load(stream).Secrets,
                Scopes = new[] { GmailService.Scope.GmailSend },
            });
        }
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}

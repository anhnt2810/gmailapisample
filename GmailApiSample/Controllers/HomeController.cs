﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using GmailApiSample.Models;
using System.IO;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Gmail.v1;
using System.Threading;
using Google.Apis.Util.Store;
using Google.Apis.Auth.OAuth2.Flows;
using Google.Apis.Auth.OAuth2.Responses;
using Google.Apis.Auth;

namespace GmailApiSample.Controllers
{
    public class HomeController : Controller
    {
        public HomeController()
        {
        }

        public IActionResult Index()
        {
            return Redirect(Program.GoogleAuthorizationCodeFlow.CreateAuthorizationCodeRequest("https://localhost:44380/Google/Authorize").Build().AbsoluteUri);
        }

        public IActionResult Privacy()
        {
            return View();
        }
    }
}
